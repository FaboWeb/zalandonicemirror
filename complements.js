var complements = [
'Every time you smile, a kitten is born.',
'If we were marooned on an island, I\'d be very sad when I had to eat you.',
'You\'re my kind of weird.',
'If we had kids, they\'d probably have super powers.',
'If I was a zombie, and hungry, I still wouldn\'t bite you.',
'Your birthday should be a national holiday. With fireworks.',
'Everyone thinks you\'re Ryan Gosling\'s handsome brother.',
'David Beckham wishes he was as popular as you.',
'You\'ve got class. Like pink cava. Or monogrammed slippers.',
'Wiser than ten Yodas you are.',
'You\'re attractive, even when you\'re ill.',
'I would follow you into battle, in a kilt, in the rain.',
'If everyone listened to you, we\'d have world peace by lunchtime.',
'And the award for daintiest feet goes to... You, and your exquisite feet.',
'You are the best man at every wedding you attend.',
'You\'re faithful, loyal and compassionate. Like a Labrador. With slightly less hair.',
'If you were a dinosaur you\'d be a Legendasaurus Rex.',
'When you move house, they\'ll put up a blue plaque.',
'At the hairdressers, people just point at you and say like that.',
'I want to be like you when I grow up.',
'Even the ladders in your tights look hot.',
'You are my most loyal and trusted advisor.',
'Your nose is beautiful. And there is so much of it to admire.',
'I\'d vote for you as All High Emperor of Earth.',
'If I was you, I\'d marry myself and start a new life with myself in Tuscany.',
'Your pupils dilate in relation to the light in the room. In a sexy way.',
'You\'ve really nailed being a human. Good job.',
'You are more adorable than a baby panda riding a sneezing piglet.',
'Your family crest should just be a big thumbs up and the word YES.',
'Your hair is like a waterfall of honey from a really good dream.',
'Your biceps are bigger than Chris Hoys thighs.',
'Your skin is smoother than an otter covered in butter.',
'If there was an air freshener that smelled like you, I\'d buy it.',
'You are the visible personification of absolute perfection.',
'When you sing, great crowds amass in the streets.',
'I\'d help you move heavy stuff any time you asked. Even if it\'s not your stuff.',
'When the sun comes up, it\'s mainly to see you.',
'Your handwriting is an art form. They should teach it in schools.',
'You taught Ray Mears how to survive in the woods.',
'You have the best dance moves. They\'re both great.',
'You are tall. I like it.',
'You alone were created on the 8th day (it took ages to get your hair just right).',
'Everything you touch should be put in a special museum.',
'You are a walking high-five.',
'You\'re more lovable than a baby hippo in a onesie.',
'If everyone carried a photo of you, there would be no more sadness.',
'You make me feel sick. In a good way.',
'I think about you when I\'m in the bath.',
'Is it a bird? Is it a plane? No. It\'s you, you massive legend.',
'Your skin is peachy, your lips are like cherries and you have a lovely pear.',
'When I\'m in need, I call you. And not only because you have a car.',
'You\'re wise and all knowing, like a mighty owl. Yet somehow, you\'re still always late.',
'I wish you were my Dad.',
'You\'re the bees knees, the cats pyjamas, the pheasants bowtie.',
'If I ever get a tattoo, it\'ll be a picture of you doing a thumbs up.',
'Everything you do should be on TV.',
'You are single handedly making male pattern baldness cool',
'I\'m going to name all my children after you, regardless of gender.',
'You have the body of Adonis and the mind of Paxman.',
'You make incredible sandwiches.',
'Without you, Earth would be a dark and lonely place.',
'You\'re cooler than The Fonz playing a guitar solo in the fridge.',
'Your skin is softer than a lamb covered in fromage frais.',
'Retro style and ironic old clothes are very in right now. Your wardrobe is probably worth millions.',
'Even the bald patches in your beard look manly.',
'You\'re the most courageous person I\'ve ever met. All your trousers are incredibly brave.',
'In a fight, you could beat two sharks, a bear and five ducks.',
'If you got splashed by a puddle, I\'d give you my trousers.',
'If you went into bat for England tomorrow, you\'d bag 70 runs. Easy.',
'The only person I\'ve ever fancied more than you is Phillip Schofield.',
'Your teeth are so clean I feel dirty just looking at them.',
'When you make tea, it\'s like a holy elixir. With biscuits.',
'I once looked at your bum. I regret nothing.',
'You are the Universes finest achievement.',
'Your eyes are like miniature galaxies.',
'When you go on holiday, everyone really misses you.',
'All future wigs should be modelled on your hair.',
'I wish you were my Mum.',
'The only person I\'ve ever fancied more than you is Jet from Gladiators.',
'I keep a picture of you in my wallet. And my sock. And on the ceiling above my bed.',
'Your mum says you\'re caring, kind, thoughtful and sweet. But she does still prefer me.',
'You are kind of heart, generous of spirit and very good at bowling.',
'You are small and I like it.',
'One day, operas will be written about your cheekbones.',
'You smell nicer than a Sunday roast.',
'You\'re successful. You\'re dependable. And yet you still owe me a tenner.',
'You\'re tougher than a granite carving of Phil Mitchell.',
'You\'re bacon. You\'re Cantona. You\'re the greatest, never change.',
'You\'re champagne. You\'re Beyonce. You\'re the greatest, never change.',
'You\'re the best looking one out of all of our friends.',
'You\'re better than the first three Star Wars films.',
'Your teeth are like peppermint scented diamonds.',
'Everyone thinks you\'re Scarlett Johansson\'s better looking sister.',
'You\'re cooler than the other side of the pillow.',
'When I first met you, I thought you were attractive and intelligent. You still look great.',
'You inspire me to write poetry. Yes, you doetry.',
'Your eyes are so shiny magpies try to steal them.',
'If were both single at 40, I think we should get married. Maybe 80 actually. Just in case.',
'You were a geek before it was cool. And you\'ll still be one when it isn\'t.',
'I\'d rob a bank for you. Seriously, I\'ve thought about it a lot.',
'You\'re the best company in the world. Once you get to know you.',
'Your cheeks are so rosy, you make roses jealous.'
];