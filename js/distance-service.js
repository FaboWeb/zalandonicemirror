(function() {
    'use strict';

    function DistanceService($rootScope, $http, $timeout) {
        var service = {};

        //var server = 'https://10.161.128.185:8080';
        var server = 'https://localhost:8080';
        var lastDistance = 300;
        var approached = false;
        var onApproach, onMovingAway;
        var errorResistance = 6;
        var errorCount = 0;
        var lastApproached = null;
        var requestTimeout = 800;
        var approachedDistance = 100;

        var evalDistance = function(distance) {
            //check for error
            if(distance > 3000) {
                if(errorCount <= errorResistance) {
                    errorCount++;
                } else {
                    if(onMovingAway) {
                        onMovingAway();
                    }
                    errorCount = 0;
                    approached = false;
                    lastDistance = distance;
                }
                return;
            } else {
                errorCount = 0;

                if (!approached && distance < approachedDistance) {
                    if (onApproach) {
                        onApproach();
                    }
                    lastApproached = new Date();
                    approached = true;
                } else if (approached && new Date(new Date().getTime() - lastApproached.getTime()).getSeconds() >= 3 && distance > approachedDistance + 50) {
                    if (onMovingAway) {
                        onMovingAway();
                    }
                    approached = false;
                }

                lastDistance = distance;
            }
        }
        /*var connection = new WebSocket('ws://localhost:8888');
        connection.onmessage = function (e) {
            console.log('Server: ' + e.data);

            evalDistance(e.data);
        };*/

        var getDistance = function(timeout) {
            $http.get(server, {
                timeout: 400
            }).then(function(e) {
                console.log('Connection server send distance: ', e.data);
                evalDistance(e.data);
            }, function (e) {
                console.log('Connection with distance server failed: ', e);
            });

            if(timeout) {
                $timeout(function() {
                    getDistance(timeout);
                }, timeout);
            }
        };

        service.onApproach = function(callback) {
            onApproach = callback;
        };
        service.onMovingAway = function(callback) {
            onMovingAway = callback;
        };

        getDistance(requestTimeout);

        return service;
    }

    angular.module('SmartMirror')
        .factory('DistanceService', DistanceService);

}());
