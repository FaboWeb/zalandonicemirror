(function(JpegCamera) {
    'use strict';

    function CameraService($timeout) {
        var service = {};
        var lastSnapshot = null;

        service.grabPhoto = function(success) {
            var camera = new JpegCamera("#camera");

            $timeout(function() {
                lastSnapshot = camera.capture({
                    shutter: false
                }); //Add options here
                lastSnapshot.show(); // Display the snapshot
                console.log('Photo taken');

                var imageUrl = lastSnapshot._canvas.toDataURL("image/jpeg");
                var imageBase64 = imageUrl.split(',')[1];

                success(imageBase64);
            },2000);
        }

        return service;
    }

    angular.module('SmartMirror')
        .factory('CameraService', CameraService);

}(window.JpegCamera));
