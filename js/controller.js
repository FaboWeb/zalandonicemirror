(function(angular) {
    'use strict';

    function MirrorCtrl(AnnyangService, CameraService, TwitterService,
    // DistanceService,
    GeolocationService, WeatherService, MapService, $scope, $timeout, $rootScope) {
        var _this = this;
        var timeouts = [];
        var defaultCountDown = 5;
        $scope.active = true;
        $scope.listening = false;
        $scope.debug = false;
        $scope.complement = ""
        $scope.focus = "default";
        $scope.user = {};

        $scope.colors=["#6ed3cf", "#9068be", "#e1e8f0", "#e62739"];

        var selectComplement = function() {
            $scope.precompliment = true;
            timeouts.push($timeout(function() {
                $scope.precompliment = false;

                var randomIndex = Math.floor((Math.random() * complements.length));
                $scope.complement = complements[randomIndex];

                timeouts.push($timeout(function() {
                    $scope.postShowingCompliment = true;
                }, 4000));
            }, 4000));
        }

        var tweetImage = function(imageBase64) {
            TwitterService.tweet($scope.complement, imageBase64, function() {

            });
            timeouts.push($timeout(function() {
                $scope.postShowingCompliment = false;
                $scope.pictureTaken = false;
            }, 5000));
        }

        var clearTimeout = null;
        var clear = function() {
            if(clearTimeout != null) {
                return;
            }

            console.debug("Clearing data");
            while(timeouts.length > 0) {
                $timeout.cancel(timeouts.pop());
            }

            $scope.$apply(function() {
                $scope.precompliment = false;
                $scope.complement = "";
                $scope.postShowingCompliment = false;
                $scope.takingPicture = false;
                $scope.pictureTaken = false;
                $scope.countDown = defaultCountDown;
            });

            clearTimeout = $timeout(function() {
                $scope.active = true;
                clearTimeout = null;
            }, 5000);
        }

        var startTimeout = null;
        var start = function(timedout) {
            if(timedout) {
                startTimeout = null;
            } else if(startTimeout != null) {
                return;
            }
            if($scope.active == false) {
                console.debug("Starting the process");

                startTimeout = $timeout(function() {
                    start(true);
                }, 1000);
            } else {
                selectComplement();
            }
        }

        var countingdown = function(callback, reset) {
            if(reset) {
                $scope.countDown = defaultCountDown;
            }
            timeouts.push($timeout(function() {
                if($scope.countDown > 0) {
                    $scope.countDown = $scope.countDown - 1;

                    //FIX
                    if($scope.countDown == 2) {
                        callback();
                    }

                    countingdown(callback);
                } else {
                    $scope.countDown = $scope.countDown - 1;
                    //callback();
                }
            }, 1000));
        }

        // DistanceService.onApproach(start);
        // DistanceService.onMovingAway(clear);

        $scope.grabPhoto = function() {
            if($scope.postShowingCompliment && !$scope.takingPicture) {
                console.debug("Starting to get a photo");

                $scope.takingPicture = true;
                console.log('Grabbing photo in 5s...');
                countingdown(function() {
                    if($scope.takingPicture) {
                        CameraService.grabPhoto(function (imageBase64) {
                            if($scope.takingPicture) {
                                $scope.takingPicture = false;
                                $scope.postShowingCompliment = false;
                                $scope.pictureTaken = true;

                                console.log('Photo ready to upload');
                                tweetImage(imageBase64);
                            }
                        });
                    }
                },true);
            }
        };

        _this.init = function() {
            //$scope.map = MapService.generateMap("Berlin,DE");
            //restCommand();
            //selectComplement();

            //Get our location and then get the weather for our location
            GeolocationService.getLocation().then(function(geoposition){
                console.log("Geoposition", geoposition);
                $scope.geoposition = geoposition;
                /*WeatherService.init(geoposition).then(function(){
                    $scope.currentForcast = WeatherService.currentForcast();
                    $scope.weeklyForcast = WeatherService.weeklyForcast();
                    console.log("Current", $scope.currentForcast);
                    console.log("Weekly", $scope.weeklyForcast);
                    //refresh the weather every hour
                    //this doesn't acutually updat the UI yet
                    //$timeout(WeatherService.refreshWeather, 3600000);
                });*/
            })

            //Initiate Hue communication
            //HueService.init();

            var defaultView = function() {
                console.debug("Ok, going to default view...");
                $scope.focus = "default";
            };

            // List commands
            var photoCommands = ['take a picture', 'take a pic', 'take picture', 'take a photo', 'take photo', 'cheese', 'tease', 'flash', 'photo'];
            var startCommands = ['start', 'mirror mirror', 'hallo spiegel'];
            var clearCommands = ['cleare', 'here', 'whipe', 'pier', 'kia'];
            for(var command in photoCommands) {
                AnnyangService.addCommand(photoCommands[command], function() {
                    $scope.grabPhoto();
                });
            };
            for(var command in startCommands) {
                AnnyangService.addCommand(startCommands[command], function() {
                    start();
                });
            };
            for(var command in clearCommands) {
                AnnyangService.addCommand(clearCommands[command], function() {
                    clear();
                });
            }

            // Fallback for all commands
            // AnnyangService.addCommand('*allSpeech', function(allSpeech) {
            //     console.debug(allSpeech);
            //     //_this.addResult(allSpeech);
            // });

            // function any(array, mapfunction) {
            //     for(var i = 0; i < array.length; i++) {
            //         if (mapfunction(array[i])) {
            //             return true;
            //         }
            //     }
            //     return false;
            // }
            var resetCommandTimeout;
            //Track when the Annyang is listening to us
            AnnyangService.start(function(listening){
                $scope.listening = listening;
            }, function(interimResult){
                // var interimResultLower = interimResult.toLowerCase();
                // let isStartCmd = any(startCommands, function(word) {
                //     return word.indexOf(interimResultLower) !== -1;
                // });
                // if (isStartCmd) {
                //     start();
                // }
                // else {
                //     let isClearCmd = any(clearCommands, function(word) {
                //         return word.indexOf(interimResultLower) !== -1;
                //     });
                //     if (isClearCmd) {
                //         clear();
                //     } else {
                //         let isPhotoCmd = any(photoCommands, function(word) {
                //             return word.indexOf(interimResultLower) !== -1;
                //         });
                //         if (isPhotoCmd)
                //             $scope.grabPhoto();
                //     }
                // }
                console.log(interimResult);
                //$scope.interimResult = interimResult;
                //$timeout.cancel(resetCommandTimeout);
            }, function(result){
                //$scope.interimResult = result[0];
                //resetCommandTimeout = $timeout(restCommand, 5000);
                //timeouts.push(resetCommandTimeout);
            });
        };


        _this.init();
    }

    angular.module('SmartMirror')
        .controller('MirrorCtrl', MirrorCtrl);

}(window.angular));
