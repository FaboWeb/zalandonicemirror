(function() {
    'use strict';

    function TwitterService($rootScope, $http) {
        var service = {};
        service.tweet = function(status, imageBase64, success) {
            var extendedStatus = status;
            var hashtags = ['#wewillhackyou', '#zalando', '#shareyourstyle', '#youdeserveit'];
            for(var i = 0; i < hashtags.length; i ++) {
                if(extendedStatus.length + hashtags[i].length + 1 <= 140) {
                    extendedStatus = extendedStatus + ' ' + hashtags[i];
                }
            }
            $http.post(TWITTER_PROXY + '/statuses/update', {
                status: status + ' #wewillhackyou #shareyourstyle',
                media: imageBase64
            }, function () {
                console.log('Uploaded photo');

                success();
            }, function() {
                console.log('Uploaded failed?!');

                success();
            });
        }

        return service;
    }

    angular.module('SmartMirror')
        .factory('TwitterService', TwitterService);

}());
