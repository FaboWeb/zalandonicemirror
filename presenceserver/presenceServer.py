from functools import wraps
import errno
import os
import signal

class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator

#!/usr/bin/python
import RPi.GPIO as GPIO
import time

#GPIO Modus (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#GPIO Pins zuweisen
GPIO_TRIGGER = 18
GPIO_ECHO = 24

#Richtung der GPIO-Pins festlegen (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

@timeout(5)
def distanz():
	# setze Trigger auf HIGH
	GPIO.output(GPIO_TRIGGER, True)

	# setze Trigger nach 0.01ms aus LOW
	time.sleep(0.00001)
	GPIO.output(GPIO_TRIGGER, False)

	StartZeit = time.time()
	StopZeit = time.time()

	# speichere Startzeit
	while GPIO.input(GPIO_ECHO) == 0:
		StartZeit = time.time()

	# speichere Ankunftszeit
	while GPIO.input(GPIO_ECHO) == 1:
		StopZeit = time.time()

	# Zeit Differenz zwischen Start und Ankunft
	TimeElapsed = StopZeit - StartZeit
	# mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
	# und durch 2 teilen, da hin und zurueck
	distanz = (TimeElapsed * 34300) / 2

	return distanz

from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import ssl
PORT_NUMBER = 8080

#This class will handles any incoming request from
#the browser 
class myHandler(BaseHTTPRequestHandler):

        def setup(self):
                BaseHTTPRequestHandler.setup(self)
                self.request.settimeout(60)
	
	#Handler for the GET requests
	def do_GET(self):
                try:
                        self.send_response(200)
                        self.send_header('Content-type','application/json')
                        self.send_header('Access-Control-Allow-Origin','*')
                        self.end_headers()
                        # Send the html message
		
                        abstand = distanz()
                        self.wfile.write("%.1f" % abstand)
                        return
                except:
                        self.send_response(500)
                        return

try:
	#Create a web server and define the handler to manage the
	#incoming request
	server = HTTPServer(('', PORT_NUMBER), myHandler)
	server.socket = ssl.wrap_socket (server.socket, certfile='/home/pi/zalandonicemirror/presenceserver/server.pem', server_side=True)
        server.socket.settimeout(10)                                 
	print 'Started httpserver on port ' , PORT_NUMBER
	
	#Wait forever for incoming htto requests
	server.serve_forever()

except KeyboardInterrupt:
	print '^C received, shutting down the web server'
	server.socket.close()
	GPIO.cleanup()
